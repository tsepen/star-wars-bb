import { createAction } from 'redux-actions';
import axios from 'axios';

import { API } from '../../constants';

import changeLocalStorage from '../../utils/changeLocalStorage';
import addBookmarks from '../../utils/addBookmarks';

const getListReceive = createAction('PEOPLE/RECEIVE');
const getListRequest = createAction('PEOPLE/REQUEST');

const getList = path => (
  async dispatch => {
    dispatch(getListReceive());

    try {
      const url = path || `${ API.V1 }/people`;

      const { data } = await axios.get(url);

      const list = addBookmarks('people', 'name', data.results)
      
      dispatch(getListRequest(list));
    } catch(err) {
      console.log(err)
    }
  }
);

const search = value => {
  return getList(`${ API.V1 }/people/?search=${ value }`)
}

const getItemRequest = createAction('HERO/REQUEST');

const getItem = id => (
  async dispatch => {
    dispatch(getListReceive());
    try {
      const { data } = await axios.get(`${ API.V1 }/people/${ id }`);

      dispatch(getItemRequest(data));
    } catch(err) {
      console.log(err)
    }
  }
);

const clearItem = createAction('HERO/CLEAR');

const clear = () => (
  dispatch => {
    dispatch(clearItem());
  }
);

const addBookmark = createAction('PEOPLE/ADD_BOOKMARK');

const setBookmark = name => (
  (dispatch, getState) => {
    const { list } = getState().people;

    const newList = list.map(item => {
      if (item.name !== name) {
        return item
      } 

      item.bookmark = !item.bookmark;

      return item
    })

    dispatch(addBookmark(newList));

    changeLocalStorage('people', name);
  }
);

export {
  getList,
  getItem,
  clear,
  setBookmark,
  search,

  getListReceive,
  getListRequest,
  getItemRequest,
  clearItem,
  addBookmark,
};