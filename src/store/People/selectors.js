const getState = state => state.people;
const isLoading = state => getState(state).isLoading;
const list = state => getState(state).list;
const item = state => getState(state).item;

export {
  getState,
  isLoading,
  list,
  item,
}