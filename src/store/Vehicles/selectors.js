const getState = state => state.vehicles;
const isLoading = state => getState(state).isLoading;
const list = state => getState(state).list;
const item = state => getState(state).item;

export {
  getState,
  isLoading,
  list,
  item,
}