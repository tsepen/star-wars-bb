import { createAction } from 'redux-actions';
import axios from 'axios';

import { API } from '../../constants';

import changeLocalStorage from '../../utils/changeLocalStorage';
import addBookmarks from '../../utils/addBookmarks';

const getListReceive = createAction('VEHICLES/RECEIVE');
const getListRequest = createAction('VEHICLES/REQUEST');

const getList = path => (
  async dispatch => {
    dispatch(getListReceive());

    try {
      const url = path || `${ API.V1 }/vehicles`;

      const { data } = await axios.get(url);

      const list = addBookmarks('vehicles', 'name', data.results)
      
      dispatch(getListRequest(list));
    } catch(err) {
      console.log(err)
    }
  }
);

const search = value => {
  return getList(`${ API.V1 }/vehicles/?search=${ value }`)
}

const getItemRequest = createAction('VEHICLE/REQUEST');

const getOneItem = id => (
  async dispatch => {
    dispatch(getListReceive());
    try {
      const { data } = await axios.get(`${ API.V1 }/vehicles/${ id }`);

      dispatch(getItemRequest(data));
    } catch(err) {
      console.log(err)
    }
  }
);

const clearItem = createAction('VEHICLE/CLEAR');

const clear = () => (
  dispatch => {
    dispatch(clearItem());
  }
);

const addBookmark = createAction('VEHICLE/ADD_BOOKMARK');

const setBookmark = name => (
  (dispatch, getState) => {
    const { list } = getState().vehicles;

    const newList = list.map(item => {
      if (item.name !== name) {
        return item
      } 

      item.bookmark = !item.bookmark;

      return item
    })

    dispatch(addBookmark(newList));

    changeLocalStorage('vehicles', name);
  }
);

export {
  getList,
  getOneItem,
  clear,
  setBookmark,
  search,

  getListReceive,
  getListRequest,
  getItemRequest,
  clearItem,
  addBookmark,
};