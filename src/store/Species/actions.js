import { createAction } from 'redux-actions';
import axios from 'axios';

import { API } from '../../constants';

import changeLocalStorage from '../../utils/changeLocalStorage';
import addBookmarks from '../../utils/addBookmarks';

const getListReceive = createAction('SPECIES/RECEIVE');
const getListRequest = createAction('SPECIES/REQUEST');

const getList = path => (
  async dispatch => {
    dispatch(getListReceive());

    try {
      const url = path || `${ API.V1 }/species`;

      const { data } = await axios.get(url);

      const list = addBookmarks('species', 'name', data.results)
      
      dispatch(getListRequest(list));
    } catch(err) {
      console.log(err)
    }
  }
);

const search = value => {
  return getList(`${ API.V1 }/species/?search=${ value }`)
}

const getItemRequest = createAction('SPECIE/REQUEST');

const getOneItem = id => (
  async dispatch => {
    dispatch(getListReceive());
    try {
      const { data } = await axios.get(`${ API.V1 }/species/${ id }`);

      dispatch(getItemRequest(data));
    } catch(err) {
      console.log(err)
    }
  }
);

const clearItem = createAction('SPECIE/CLEAR');

const clear = () => (
  dispatch => {
    dispatch(clearItem());
  }
);

const addBookmark = createAction('SPECIE/ADD_BOOKMARK');

const setBookmark = name => (
  (dispatch, getState) => {
    const { list } = getState().species;

    const newList = list.map(item => {
      if (item.name !== name) {
        return item
      } 

      item.bookmark = !item.bookmark;

      return item
    })

    dispatch(addBookmark(newList));

    changeLocalStorage('species', name);
  }
);

export {
  getList,
  getOneItem,
  clear,
  setBookmark,
  search,

  getListReceive,
  getListRequest,
  getItemRequest,
  clearItem,
  addBookmark,
};