import { handleActions } from 'redux-actions';

import {
  getListReceive,
  getListRequest,
  getItemRequest,
  clearItem,
  addBookmark,
} from './actions';

const initialState = {
  isLoading: false,
  item: null,
  list: null,
};

export default handleActions({
  [ getListReceive ]: (state) => ({
    ...state, isLoading: true
  }),
  [ getListRequest ]: (state, { payload }) => ({
    ...state, isLoading: false, list: payload
  }),
  [ getItemRequest ]: (state, { payload }) => ({
    ...state, isLoading: false, item: payload
  }),
  [ clearItem ]: (state) => ({
    ...state, item: null
  }),
  [ addBookmark ]: (state, { payload }) => ({
    ...state, list: payload
  }),
}, initialState);
