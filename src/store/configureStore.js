import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import films from '../store/Films';
import people from '../store/People';
import planets from '../store/Planets';
import starships from '../store/Starships';
import species from '../store/Species';
import vehicles from '../store/Vehicles';

const initialState = {};
const loggerMiddleware = createLogger();

// Redux DevTools extension code
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : e => e;

export const history = createHistory();

const configureStore = () => {
  const rootReducer = combineReducers({
    films,
    people,
    planets,
    starships,
    species,
    vehicles,
    router: routerReducer,
  });

  const enhancer = composeEnhancers(applyMiddleware(
    routerMiddleware(history),
    thunkMiddleware,
    loggerMiddleware,
  ));

  return createStore(rootReducer, initialState, enhancer);
};

export default configureStore;