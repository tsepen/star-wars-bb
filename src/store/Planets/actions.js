import { createAction } from 'redux-actions';
import axios from 'axios';

import { API } from '../../constants';

import changeLocalStorage from '../../utils/changeLocalStorage';
import addBookmarks from '../../utils/addBookmarks';

const getListReceive = createAction('PLANETS/RECEIVE');
const getListRequest = createAction('PLANETS/REQUEST');

const getList = path => (
  async dispatch => {
    dispatch(getListReceive());

    try {
      const url = path || `${ API.V1 }/planets`;

      const { data } = await axios.get(url);

      const list = addBookmarks('planets', 'name', data.results)
      
      dispatch(getListRequest(list));
    } catch(err) {
      console.log(err)
    }
  }
);

const search = value => {
  return getList(`${ API.V1 }/planets/?search=${ value }`)
}

const getItemRequest = createAction('PLANET/REQUEST');

const getOneItem = id => (
  async dispatch => {
    dispatch(getListReceive());
    try {
      const { data } = await axios.get(`${ API.V1 }/planets/${ id }`);

      dispatch(getItemRequest(data));
    } catch(err) {
      console.log(err)
    }
  }
);

const clearItem = createAction('PLANET/CLEAR');

const clear = () => (
  dispatch => {
    dispatch(clearItem());
  }
);

const addBookmark = createAction('PLANET/ADD_BOOKMARK');

const setBookmark = name => (
  (dispatch, getState) => {
    const { list } = getState().planets;

    const newList = list.map(item => {
      if (item.name !== name) {
        return item
      } 

      item.bookmark = !item.bookmark;

      return item
    })

    dispatch(addBookmark(newList));

    changeLocalStorage('planets', name);
  }
);

export {
  getList,
  getOneItem,
  clear,
  setBookmark,
  search,

  getListReceive,
  getListRequest,
  getItemRequest,
  clearItem,
  addBookmark,
};