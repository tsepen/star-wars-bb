import { createAction } from 'redux-actions';
import axios from 'axios';

import { API } from '../../constants';

import changeLocalStorage from '../../utils/changeLocalStorage';
import addBookmarks from '../../utils/addBookmarks';

const getListReceive = createAction('FILMS/RECEIVE');
const getListRequest = createAction('FILMS/REQUEST');

const getList = path => (
  async dispatch => {
    dispatch(getListReceive());

    try {
      const url = path || `${ API.V1 }/films`;

      const { data } = await axios.get(url);

      const list = addBookmarks('films', 'episode_id', data.results)
      
      dispatch(getListRequest(list));
    } catch(err) {
      console.log(err)
    }
  }
);

const search = value => {
  return getList(`${ API.V1 }/films/?search=${ value }`)
}

const getItemRequest = createAction('FILM/REQUEST');

const getOneItem = id => (
  async dispatch => {
    dispatch(getListReceive());
    try {
      const { data } = await axios.get(`${ API.V1 }/films/${ id }`);

      dispatch(getItemRequest(data));
    } catch(err) {
      console.log(err)
    }
  }
);

const clearItem = createAction('FILM/CLEAR');

const clear = () => (
  dispatch => {
    dispatch(clearItem());
  }
);

const addBookmark = createAction('FILM/ADD_BOOKMARK');

const setBookmark = episode => (
  (dispatch, getState) => {
    const films = getState().films.list;

    const newList = films.map(item => {
      if (item.episode_id !== episode) {
        return item
      } 

      item.bookmark = !item.bookmark;

      return item
    })

    dispatch(addBookmark(newList));

    changeLocalStorage('films', episode);
  }
);

export {
  getList,
  getOneItem,
  clear,
  setBookmark,
  search,

  getListReceive,
  getListRequest,
  getItemRequest,
  clearItem,
  addBookmark,
};