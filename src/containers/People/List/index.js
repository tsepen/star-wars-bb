import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as selectors from '../../../store/People/selectors';
import { getList, search, setBookmark as setBookmarkAction } from '../../../store/People/actions';

import Item from '../Item'
import Preloader from '../../../components/Preloader';
import Search from '../../../components/Search';

import getId from '../../../utils/getIdFromUrl'

import style from './style.css';

/**
 * People list
 */
class List extends PureComponent {
  state = {
    input: '',
  }

  componentDidMount() {
    this.props.getList();
  }

  inputChange = e => {
    this.setState({ input: e.target.value });
  }

  search = () => {
    this.props.search(this.state.input);
  }

  reset = () => {
    this.setState({ input: '' });
    this.props.getList();
  }

  render() {
    const { isLoading, list, setBookmark } = this.props;
    
    if (isLoading || !list) {
      return <Preloader />
    }
  
    return (
      <Fragment>
        <Search
          placeholder="Hero name"
          value={ this.state.input }
          onChange={ this.inputChange }
          onSearch={ this.search }
          onReset={ this.reset }
        />
        <div className={ style.list }>
          {
            list.map(item => (
              <Item
                key={ item.episode_id }
                item={ item }
                id={ getId(item.url) }
                setBookmark={ setBookmark }
              />
            ))
          }
        </div>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getList,
  search,
  setBookmark: setBookmarkAction,
}, dispatch);

const mapStateToProps = state => ({
  list: selectors.list(state),
  isLoading: selectors.isLoading(state),
});

List.propTypes = {
  getList: PropTypes.func.isRequired,
  setBookmark: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  list: PropTypes.instanceOf(Array),
}

export default connect(mapStateToProps, mapDispatchToProps)(List);