import React from 'react';
import PropTypes from 'prop-types';

import { ROUTES } from '../../../constants';

import { Link } from 'react-router-dom';

import style from './style.css';

import icon from '../../../assets/images/bookmark.svg';
import iconActive from '../../../assets/images/bookmark_active.svg';

/**
 * One item of people list
 */
const Item = ({
  item,
  id,
  setBookmark,
}) => {
  const img = item.bookmark ? iconActive : icon;

  return (
    <article className={ style.item }>
      <img 
        src={ img }
        alt="bookmark"
        className={ style.icon }
        onClick={ () => setBookmark(item.name) }
      />
      <Link to={ `${ ROUTES.PEOPLE }/${ id }` }>
        <h2 className={ style.name }>{item.name}</h2>
      </Link>
      <p className={ style.height }>Height: {item.height}</p>  
      <p className={ style.mass }>Mass: {item.mass}</p>
      <p className={ style.color }>Hair color: {item.hair_color}</p>  
      <p className={ style.color }>Skin color: {item.skin_color}</p> 
      <p className={ style.color }>Eye color: {item.eye_color}</p>
      <p className={ style.year }>Birth year: {item.birth_year}</p>  
      <p className={ style.gender }>Gender: {item.gender}</p>
    </article>
  );
} 

Item.propTypes = {
  item: PropTypes.instanceOf(Object).isRequired,
  id: PropTypes.string.isRequired,
  setBookmark: PropTypes.func.isRequired,
};

export default Item;