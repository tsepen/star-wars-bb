import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as selectors from '../../../store/People/selectors';
import { getItem, clear } from '../../../store/People/actions';

import Preloader from '../../../components/Preloader';
import Helmet from '../../../components/Helmet';
import Title from '../../../components/Title';

import style from './style.css';

/**
 * Hero page container
 */
class Film extends PureComponent {
  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.getItem(id);
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const { isLoading, item } = this.props;

    if (isLoading || !item) {
      return <Preloader />
    }

    if (item) {
      return (
        <section className={ style.content }>
          <Helmet title={ item.name } />
          <Title title={ item.name } />
          <p>Height: {item.height}</p>  
          <p>Mass: {item.mass}</p> 
          <p>Hair color: {item.hair_color}</p>  
          <p>Skin color: {item.skin_color}</p> 
          <p>Eye color: {item.eye_color}</p>
          <p>Birth year: {item.birth_year}</p>  
          <p>Gender: {item.gender}</p>
        </section>
      )
    }

    return false
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getItem,
  clear,
}, dispatch);

const mapStateToProps = state => ({
  item: selectors.item(state),
  isLoading: selectors.isLoading(state),
});

Film.propTypes = {
  getItem: PropTypes.func.isRequired,
  item: PropTypes.instanceOf(Object),
  clear: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Film));