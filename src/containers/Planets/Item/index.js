import React from 'react';
import PropTypes from 'prop-types';

import { ROUTES } from '../../../constants';

import { Link } from 'react-router-dom';

import style from './style.css';

import icon from '../../../assets/images/bookmark.svg';
import iconActive from '../../../assets/images/bookmark_active.svg';

/**
 * One item of planets list
 */
const Item = ({
  item,
  id,
  setBookmark,
}) => {
  const img = item.bookmark ? iconActive : icon;

  return (
    <article className={ style.item }>
      <img 
        src={ img }
        alt="bookmark"
        className={ style.icon }
        onClick={ () => setBookmark(item.name) }
      />
      <Link to={ `${ ROUTES.PLANETS }/${ id }` }>
        <h2 className={ style.name }>{item.name}</h2>
      </Link>
      <p className={ style.text }>Rotation period: {item.rotation_period}</p>  
      <p className={ style.text }>Orbital period: {item.orbital_period}</p> 
    </article>
  );
} 

Item.propTypes = {
  item: PropTypes.instanceOf(Object).isRequired,
  id: PropTypes.string.isRequired,
  setBookmark: PropTypes.func.isRequired,
};

export default Item;