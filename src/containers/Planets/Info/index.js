import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as selectors from '../../../store/Planets/selectors';
import { getOneItem, clear } from '../../../store/Planets/actions';

import Preloader from '../../../components/Preloader';
import Helmet from '../../../components/Helmet';
import Title from '../../../components/Title';

import style from './style.css';

/**
 * Planet page container
 */
class Item extends PureComponent {
  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.getOneItem(id);
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const { isLoading, item } = this.props;

    if (isLoading || !item) {
      return <Preloader />
    }

    if (item) {
      return (
        <section className={ style.content }>
          <Helmet title={ item.name } />
          <Title title={ item.name } />
          <p>Rotation period: {item.rotation_period}</p>  
          <p>Orbital period: {item.orbital_period}</p> 
          <p>Diameter: {item.diameter}</p>  
          <p>Climate: {item.climate}</p> 
          <p>Gravity: {item.gravity}</p>
          <p>Terrain: {item.terrain}</p>  
          <p>Surface water: {item.surface_water}</p>
          <p>Population: {item.population}</p>
        </section>
      )
    }

    return false
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getOneItem,
  clear,
}, dispatch);

const mapStateToProps = state => ({
  item: selectors.item(state),
  isLoading: selectors.isLoading(state),
});

Item.propTypes = {
  getOneItem: PropTypes.func.isRequired,
  item: PropTypes.instanceOf(Object),
  clear: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Item));