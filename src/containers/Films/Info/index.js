import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as selectors from '../../../store/Films/selectors';
import { getOneItem, clear } from '../../../store/Films/actions';

import Preloader from '../../../components/Preloader';
import Helmet from '../../../components/Helmet';
import Title from '../../../components/Title';

import style from './style.css';

/**
 * Film page container
 */
class Film extends PureComponent {
  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.getOneItem(id);
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const { isLoading, item } = this.props;

    if (isLoading || !item) {
      return <Preloader />
    }

    if (item) {
      return (
        <section className={ style.content }>
          <Helmet title={ item.title } />
          <Title title={ item.title } />
          <p>{item.opening_crawl}</p>
          <p>Episode: {item.episode_id}</p>  
          <p>Director: {item.director}</p>  
          <p>Producer: {item.producer}</p> 
          <p>Release: {item.release_date}</p> 
        </section>
      )
    }

    return false
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getOneItem,
  clear,
}, dispatch);

const mapStateToProps = state => ({
  item: selectors.item(state),
  isLoading: selectors.isLoading(state),
});

Film.propTypes = {
  getOneItem: PropTypes.func.isRequired,
  item: PropTypes.instanceOf(Object),
  clear: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Film));