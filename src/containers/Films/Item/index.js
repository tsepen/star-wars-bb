import React from 'react';
import PropTypes from 'prop-types';

import { ROUTES } from '../../../constants';

import { Link } from 'react-router-dom';

import style from './style.css';

import icon from '../../../assets/images/bookmark.svg';
import iconActive from '../../../assets/images/bookmark_active.svg';

/**
 * One item of films list
 */
const Item = ({
  id,
  item,
  setBookmark,
}) => {
  const img = item.bookmark ? iconActive : icon;

  return (
    <article className={ style.item }>
      <img 
        src={ img }
        alt="bookmark"
        className={ style.icon }
        onClick={ () => setBookmark(item.episode_id) }
      />
      <Link to={ `${ ROUTES.FILMS }/${ id }` }>
        <h2 className={ style.name }>{item.title}</h2>
      </Link>
      <p className={ style.desc }>{item.opening_crawl}</p>
      <p className={ style.episode }>Episode: {item.episode_id}</p>  
      <p className={ style.director }>Director: {item.director}</p>  
      <p className={ style.producer }>Producer: {item.producer}</p> 
      <p className={ style.relese }>Release: {item.relese}</p>  
    </article>
  );
} 

Item.propTypes = {
  id: PropTypes.string.isRequired,
  item: PropTypes.instanceOf(Object).isRequired,
  setBookmark: PropTypes.func.isRequired,
};

export default Item;