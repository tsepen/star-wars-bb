import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as selectors from '../../../store/Vehicles/selectors';
import { getOneItem, clear } from '../../../store/Vehicles/actions';

import Preloader from '../../../components/Preloader';
import Helmet from '../../../components/Helmet';
import Title from '../../../components/Title';

import style from './style.css';

/**
 * Vehicles page container
 */
class Item extends PureComponent {
  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.getOneItem(id);
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const { isLoading, item } = this.props;

    if (isLoading || !item) {
      return <Preloader />
    }

    if (item) {
      return (
        <section className={ style.content }>
          <Helmet title={ item.name } />
          <Title title={ item.name } />
          <p>Model: {item.model}</p>  
          <p>Manufacturer: {item.manufacturer}</p> 
          <p>Cost in credits: {item.cost_in_credits}</p>  
          <p>Length: {item.length}</p> 
          <p>Max atmosphering speed: {item.max_atmosphering_speed}</p>
          <p>Crew: {item.crew}</p>  
          <p>Passengers: {item.passengers}</p>
          <p>Cargo capacity: {item.cargo_capacity}</p>
          <p>Vehicle class: {item.vehicle_class}</p>
        </section>
      )
    }

    return false
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getOneItem,
  clear,
}, dispatch);

const mapStateToProps = state => ({
  item: selectors.item(state),
  isLoading: selectors.isLoading(state),
});

Item.propTypes = {
  getOneItem: PropTypes.func.isRequired,
  item: PropTypes.instanceOf(Object),
  clear: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Item));