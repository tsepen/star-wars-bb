import React, { PureComponent } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as selectors from '../../../store/Species/selectors';
import { getOneItem, clear } from '../../../store/Species/actions';

import Preloader from '../../../components/Preloader';
import Helmet from '../../../components/Helmet';
import Title from '../../../components/Title';

import style from './style.css';

/**
 * Species page container
 */
class Item extends PureComponent {
  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.getOneItem(id);
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const { isLoading, item } = this.props;

    if (isLoading || !item) {
      return <Preloader />
    }

    if (item) {
      return (
        <section className={ style.content }>
          <Helmet title={ item.name } />
          <Title title={ item.name } />
          <p>Classification: {item.classification}</p>  
          <p>Designation: {item.designation}</p> 
          <p>Average height: {item.average_height}</p>  
          <p>Skin colors: {item.skin_colors}</p> 
          <p>Hair colors: {item.hair_colors}</p>
          <p>Eye colors: {item.eye_colors}</p>  
          <p>Average lifespan: {item.average_lifespan}</p>
          <p>Language: {item.language}</p>
        </section>
      )
    }

    return false
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getOneItem,
  clear,
}, dispatch);

const mapStateToProps = state => ({
  item: selectors.item(state),
  isLoading: selectors.isLoading(state),
});

Item.propTypes = {
  getOneItem: PropTypes.func.isRequired,
  item: PropTypes.instanceOf(Object),
  clear: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Item));