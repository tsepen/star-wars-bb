export default (type, name, list) => {
  const bookmarks = localStorage.getItem(type) || [];

  const newList = list.map(item => {
    const index = bookmarks.indexOf(item[ name ]);

    if (index !== -1) {
      item.bookmark = true;

      return item
    }

    return item;
  });

  return newList;
}