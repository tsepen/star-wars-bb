export default (list, id) => {
  const fromLocalStorage = JSON.parse(localStorage.getItem(list)) || [];

  const index = fromLocalStorage.indexOf(id);

  if (index === -1) {
    fromLocalStorage.push(id)
  } else {
    fromLocalStorage.splice(index, 1);
  }

  localStorage.setItem(list, JSON.stringify(fromLocalStorage))
}