export default (url) => {
  const array = url.split('/');
  const length = array.length;

  return array[ length - 2 ];
}