import React from 'react';

import PageInfo from '../../components/PageInfo';
import Info from '../../containers/Planets/Info';

/**
 * Planets info page.
 */
export default () => (
  <PageInfo >
    <Info />
  </PageInfo >
)