import React from 'react';

import Page from '../../components/Page';
import List from '../../containers/Planets/List'

/**
 * Planets page.
 */
export default () => (
  <Page title="Planets" >
    <List />
  </Page>
)