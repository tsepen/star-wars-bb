import React from 'react';

import { ROUTES } from '../../constants';

import { Link } from 'react-router-dom';

import Helmet from '../../components/Helmet';

import styles from './styles.css';

const links = [
  {
    name: 'Films',
    url: ROUTES.FILMS,
  },
  {
    name: 'People',
    url: ROUTES.PEOPLE,
  },
  {
    name: 'Planets',
    url: ROUTES.PLANETS,
  },
  {
    name: 'Starships',
    url: ROUTES.STARSHIPS,
  },
  {
    name: 'Species',
    url: ROUTES.SPECIES,
  },
  {
    name: 'Vehicles',
    url: ROUTES.VEHICLES,
  },
]

export default () => (
  <div className={ styles.nav }>
    <Helmet title="Star Wars - Home"/>
    {
      links.map(item => (
        <Link 
          key={ item.name }
          to={ item.url }
          className={ styles.link }
        >
          { item.name }
        </Link> 
      ))
    }
  </div>
)
