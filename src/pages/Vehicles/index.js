import React from 'react';

import Page from '../../components/Page';
import List from '../../containers/Vehicles/List'

/**
 * Vehicles page.
 */
export default () => (
  <Page title="Vehicles" >
    <List />
  </Page>
)