import React from 'react';

import PageInfo from '../../components/PageInfo';
import Info from '../../containers/Vehicles/Info';

/**
 * Vehicles info page.
 */
export default () => (
  <PageInfo >
    <Info />
  </PageInfo >
)