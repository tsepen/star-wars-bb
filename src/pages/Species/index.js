import React from 'react';

import Page from '../../components/Page';
import List from '../../containers/Species/List'

/**
 * Species page.
 */
export default () => (
  <Page title="Species" >
    <List />
  </Page>
)