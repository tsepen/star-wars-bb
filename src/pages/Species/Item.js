import React from 'react';

import PageInfo from '../../components/PageInfo';
import Info from '../../containers/Species/Info';

/**
 * Species info page.
 */
export default () => (
  <PageInfo >
    <Info />
  </PageInfo >
)