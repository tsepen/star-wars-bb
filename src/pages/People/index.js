import React from 'react';

import Page from '../../components/Page';
import List from '../../containers/People/List'

/**
 * People page.
 */
export default () => (
  <Page title="People" >
    <List />
  </Page>
)