import React from 'react';

import PageInfo from '../../components/PageInfo';
import Info from '../../containers/People/Info';

/**
 * People info page.
 */
export default () => (
  <PageInfo >
    <Info />
  </PageInfo >
)