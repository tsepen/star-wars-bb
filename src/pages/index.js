import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './Home';
import Films from './Films';
import People from './People';
import Planets from './Planets';
import Species from './Species';
import Starships from './Starships';
import Vehicles from './Vehicles';
import FilmInfo from './Films/Item';
import Hero from './People/Item';
import Planet from './Planets/Item';
import Starship from './Starships/Item';
import Specie from './Species/Item';
import Vehicle from './Vehicles/Item';

import NotFoundPage from '../components/NotFoundPage';

import { ROUTES } from '../constants';

const Root = ({ store }) => (
  <Provider store={ store }>
    <Router>
      <Switch>
        <Route exact path={ ROUTES.FILMS } component={ Films } />
        <Route exact path={ ROUTES.PEOPLE } component={ People } />
        <Route exact path={ ROUTES.PLANETS } component={ Planets } />
        <Route exact path={ ROUTES.SPECIES } component={ Species } />
        <Route exact path={ ROUTES.STARSHIPS } component={ Starships } />
        <Route exact path={ ROUTES.VEHICLES } component={ Vehicles } />

        <Route path={ ROUTES.FILM } component={ FilmInfo } />
        <Route path={ ROUTES.HERO } component={ Hero } />
        <Route path={ ROUTES.PLANET } component={ Planet } />
        <Route path={ ROUTES.STARSHIP } component={ Starship } />
        <Route path={ ROUTES.SPECIE } component={ Specie } />
        <Route path={ ROUTES.VEHICLE } component={ Vehicle } />
        <Route exact path={ ROUTES.HOME } component={ Home } />
        <Route component={ NotFoundPage } />
      </Switch>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.instanceOf(Object).isRequired,
};

export default Root;