import React from 'react';

import PageInfo from '../../components/PageInfo';
import Info from '../../containers/Films/Info';

/**
 * Film info page.
 */
export default () => (
  <PageInfo >
    <Info />
  </PageInfo >
)