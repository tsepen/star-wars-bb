import React from 'react';

import Page from '../../components/Page';
import List from '../../containers/Films/List'

/**
 * Films page.
 */
export default () => (
  <Page title="Films" >
    <List />
  </Page>
)