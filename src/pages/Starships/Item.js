import React from 'react';

import PageInfo from '../../components/PageInfo';
import Info from '../../containers/Starships/Info';

/**
 * Starships info page.
 */
export default () => (
  <PageInfo >
    <Info />
  </PageInfo >
)