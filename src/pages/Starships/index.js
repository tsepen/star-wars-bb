import React from 'react';

import Page from '../../components/Page';
import List from '../../containers/Starships/List'

/**
 * Starships page.
 */
export default () => (
  <Page title="Starships" >
    <List />
  </Page>
)