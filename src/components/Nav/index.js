import React from 'react';

import { ROUTES } from '../../constants';

import { NavLink } from 'react-router-dom';

import styles from './styles.css';

/**
 * Main navigation menu
 */
const menu = [
  {
    name: 'Home',
    url: ROUTES.HOME 
  },
  {
    name: 'Films',
    url: ROUTES.FILMS 
  },
  {
    name: 'People',
    url: ROUTES.PEOPLE 
  },
  {
    name: 'Planets',
    url: ROUTES.PLANETS 
  },
  {
    name: 'Starships',
    url: ROUTES.STARSHIPS 
  },
  {
    name: 'Vehicles',
    url: ROUTES.VEHICLES 
  },
]

export default () => (
  <nav className={ styles.nav }>
    {menu.map(item => (
      <NavLink
        key={ item.name }
        exact to={ item.url }
        className={ styles.link }
        activeClassName={ styles.active }
      >
        {item.name}
      </NavLink>
    ))}
  </nav>
);
