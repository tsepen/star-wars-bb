import React, { Fragment } from 'react';

import Title from '../../components/Title';
import Helmet from '../../components/Helmet';
import Nav from '../Nav/';

/**
 * Page component.
 */
const Page = ({ title, children }) => (
  <Fragment>
    <Helmet title={ `${ title } - Star Wars` } />
    <Nav />
    <Title title={ title } />
    { children }
  </Fragment>
)

export default Page;