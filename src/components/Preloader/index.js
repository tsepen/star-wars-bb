import React from 'react';
import classnames from 'classnames';

import style from './style.css';

export default () => (
  <div className={ style.circle }>
    <div className={ classnames(style.circle1, style.child) } />
    <div className={ classnames(style.circle2, style.child) } />
    <div className={ classnames(style.circle3, style.child) } />
    <div className={ classnames(style.circle4, style.child) } />
    <div className={ classnames(style.circle5, style.child) } />
    <div className={ classnames(style.circle6, style.child) } />
    <div className={ classnames(style.circle7, style.child) } />
    <div className={ classnames(style.circle8, style.child) } />
    <div className={ classnames(style.circle9, style.child) } />
    <div className={ classnames(style.circle10, style.child) } />
    <div className={ classnames(style.circle11, style.child) } />
    <div className={ classnames(style.circle12, style.child) } />
  </div>
);
