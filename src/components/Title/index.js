import React from 'react';
import PropTypes from 'prop-types';

import style from './style.css'

/**
 * Main title on page
 */
const Title = ({ title }) => (
  <h1 className={ style.title } >{title}</h1>
);

Title.propTypes = {
  title: PropTypes.string.isRequired,
}

export default Title;