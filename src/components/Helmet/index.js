import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

const HelmetComponent = ({ title }) => (
  <Helmet>
    <meta charSet="utf-8" />
    <title>{title}</title>
  </Helmet>
)

HelmetComponent.propTypes = {
  title: PropTypes.string.isRequired,
}

export default HelmetComponent