import React from 'react';
import PropTypes from 'prop-types';

import style from './style.css';

/**
 * Search component
 */
const Search = ({ value, onChange, onSearch, onReset,  placeholder }) => (
  <div className={ style.search }>
    <input
      className={ style.input }
      value={ value }
      onChange={ onChange }
      placeholder={ placeholder }
    />
    <button
      className={ style.button }
      onClick={ onSearch }
    >
      Search
    </button>
    <button
      className={ style.button }
      onClick={ onReset }
    >
      Reset
    </button>
  </div>
);

Search.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
}

export default Search;