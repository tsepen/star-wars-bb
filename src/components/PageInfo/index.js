import React, { Fragment } from 'react';

import Nav from '../Nav/';

/**
 * Page info component.
 */
const Page = ({ children }) => (
  <Fragment>
    <Nav />
    { children }
  </Fragment>
)

export default Page;