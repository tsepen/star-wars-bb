# BB - Star Wars

In the project directory:

1) Install dependencies

```
npm install
```
2) Run

```
npm start
```
Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.


The API resource - https://swapi.co/documentation

What app doing:
1. Show the results from the API call for any resource (please implement as many resources as you can).
2. Each resource should be shown on it's own tab (please use a router for that and save a tab name in the url).
3. Implement the ability of search by name and pagination.
4. User has an ability to make some note for any resource's item, it should be look like tooltip under the item. All the notes should be restored after browser page reloading.
